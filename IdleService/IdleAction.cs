﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdleService
{
    public class IdleAction
    {
        public class Settings
        {
            public Settings()
            {
                IdleActionUrls = new List<string>();
                ActiveActionUrls = new List<string>();
            }
            private long _idleThreshold;
            public long IdleThreshold
            {
                get
                {
                    return _idleThreshold;
                }

                set
                {
                    if (value <= 0)
                        value = 0;
                    _idleThreshold = value;
                }
            }
            private List<string> _idleActionUrls = new List<string>();

            public List<string> IdleActionUrls { get => _idleActionUrls; set => _idleActionUrls = ExpandUrls(value); }
            private List<string> _activeActionUrls = new List<string>();
            public List<string> ActiveActionUrls { get => _activeActionUrls; set => _activeActionUrls = ExpandUrls(value); }

            private List<string> ExpandUrls(List<string> urls)
            {
                var updatedList = new List<string>();

                const string envMarker = "{env:";
                foreach (var s in urls)
                {
                    var index = s.IndexOf(envMarker);

                    if (index < 0)
                    {
                        updatedList.Add(s);
                        continue;
                    }

                    var lastIndex = s.IndexOf("}", index);
                    if (lastIndex < 0)
                        throw new ArgumentOutOfRangeException("s", "Cannot find closing }.  The URL format is invalid.  It should be {env:someVariable}.");

                    var identifier = s.Substring(index + envMarker.Length, lastIndex - index - envMarker.Length);

                    var identifierValue = Environment.GetEnvironmentVariable(identifier);

                    if (identifierValue == null)
                    {
                        updatedList.Add(s);
                        continue;
                    }


                    var newUrl = s.Substring(0, index) + identifierValue + s.Substring(lastIndex + 1);
                    updatedList.Add(newUrl);
                }

                return updatedList;
            }
        }

        public virtual Settings MySettings { get; }
        //has this state been triggered
        public virtual bool IsCurrentlyIdle { get; private set; }
        private bool _initalized = false;

        public virtual string Name { get; set; }

        private IHttpActionExecutorFactory _actionFactory;
        private ILogger<IdleAction> _logger;

        public IdleAction(IOptions<Settings> options, IHttpActionExecutorFactory actionFactory, ILogger<IdleAction> logger)
        {
            MySettings = options.Value;
            _actionFactory = actionFactory;
            _logger = logger;
        }

        private bool ShouldChangeState(TimeSpan userIdleTime, out bool newIsIdleState)
        {

            if ((IsCurrentlyIdle == false || !_initalized) && userIdleTime.TotalMilliseconds >= MySettings.IdleThreshold)
            {
                _logger.LogInformation("Action group {groupName} initialized {initialized} idletime {idleTime} changing to idle state.", Name, _initalized, userIdleTime);
                _initalized = true;
                newIsIdleState = true;
                return true;
            }

            if ((IsCurrentlyIdle == true || !_initalized) && userIdleTime.TotalSeconds <= 0)
            {
                _initalized = true;
                _logger.LogInformation("Action group {groupName} initialized {initialized} idletime {idleTime} changing to active state.", Name, _initalized, userIdleTime);
                newIsIdleState = false;
                return true;
            }

            _logger.LogDebug("Action group {groupName} initialized {initialized} idletime {idleTime} default no trigger.", Name, userIdleTime, _initalized, userIdleTime);

            newIsIdleState = false;
            return false;
        }

        public virtual async Task ProcessIdleTime(TimeSpan userIdleTime)
        {
            bool hasErrored = false;

            if (!ShouldChangeState(userIdleTime, out bool newIsIdleState))
            {
                return;
            }

            try
            {
                IList<string> urls = newIsIdleState ? MySettings.IdleActionUrls : MySettings.ActiveActionUrls;

                foreach (var url in urls)
                {
                    await _actionFactory.GetHttpActionExecutor().GetAsync(url);
                }
            }
            catch (Exception)
            {
                hasErrored = true;
            }

            if (!hasErrored)
            {
                IsCurrentlyIdle = newIsIdleState;
                _logger.LogDebug("Action group {groupName} executed successfully.", Name);
            }
            else
                _logger.LogError("Action group {groupName} failed to execute successfully.", Name);
        }
    }
}
