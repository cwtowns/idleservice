﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleService
{
    public static class ServicesConfiguration
    {
        public static void AddIdleService(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<Worker.Settings>(options => config.GetSection("service").Bind(options));
            services.Configure<IdleActionFactory.Settings>(options => config.GetSection("service").Bind(options));

            services.AddSingleton<IHttpActionExecutorFactory, HttpActionExecutorFactory>();
            services.AddSingleton<IdleActionFactory, IdleActionFactory>();
            services.AddSingleton<IdleStateFactory, IdleStateFactory>();
            services.AddSingleton<IdleTimeFactory, RealIdleTimeFactory>();
            services.AddSingleton<Worker, Worker>();
            //services.AddHostedService<Worker>();
        }
    }
}
