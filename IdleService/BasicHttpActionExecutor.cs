﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IdleService
{
    public interface IHttpActionExectuor
    {
        Task GetAsync(string url);
    }

    public class BasicHttpActionExecutor : IHttpActionExectuor
    {
        private WebClient _webClient = new WebClient();
        private ILogger<BasicHttpActionExecutor> _logger;

        public BasicHttpActionExecutor(ILogger<BasicHttpActionExecutor> logger)
        {
            _logger = logger;
        }


        public async Task ExecuteRawGetAsync(string url)
        {
            await _webClient.OpenReadTaskAsync(new Uri(url));
        }

        public async Task GetAsync(string url)
        {
            try
            {
                _logger.LogDebug("Fetching {url}.", url);
                await ExecuteRawGetAsync(url);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unable to execute web request for {url}", url);
                throw;
            }
        }
    }
}
