using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace IdleService
{
    public class Worker : BackgroundService
    {
        public class Settings
        {
            public int PollingInterval { get; set; }
        }

        private readonly ILogger<Worker> _logger;
        private readonly IdleStateFactory _idleStateFactory;
        private readonly IList<IdleAction> _actions;
        private Settings _settings;

        public Worker(ILogger<Worker> logger, IdleStateFactory idleStateFactory, IdleActionFactory actionFactory, IOptions<Settings> settings)
        {
            _logger = logger;
            _idleStateFactory = idleStateFactory;
            _settings = settings.Value;
            _actions = actionFactory.GetActions();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Startup");
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogDebug("Worker running at: {time}", DateTimeOffset.Now);

                var idleTime = _idleStateFactory.GetIdleTime();

                foreach (var action in _actions)
                {
                    await action.ProcessIdleTime(idleTime);
                }

                await Task.Delay(_settings.PollingInterval, stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Shutdown");

            await base.StopAsync(stoppingToken);
        }
    }
}
