﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IdleService
{
    public interface IdleTimeFactory
    {
        TimeSpan IdleTime { get; }
    }

    public class RealIdleTimeFactory : IdleTimeFactory
    {
        private ILogger<RealIdleTimeFactory> _logger;

        public RealIdleTimeFactory(ILogger<RealIdleTimeFactory> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        [DllImport("user32.dll", SetLastError = false)]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);
        [StructLayout(LayoutKind.Sequential)]
        private struct LASTINPUTINFO
        {
            public uint cbSize;
            public int dwTime;
        }
        private DateTime LastInput
        {
            get
            {
                DateTime bootTime = DateTime.UtcNow.AddMilliseconds(-Environment.TickCount);
                DateTime lastInput = bootTime.AddMilliseconds(LastInputTicks);
                _logger.LogDebug("TickCount is {tickCount} Boottime is {bootTime} and LastInputTicks is {LastInputTicks}", Environment.TickCount, bootTime.ToString(), LastInputTicks);
                return lastInput;
            }
        }
        public virtual TimeSpan IdleTime
        {
            get
            {
                return DateTime.UtcNow.Subtract(LastInput);
            }
        }
        private int LastInputTicks
        {
            get
            {
                LASTINPUTINFO lii = new LASTINPUTINFO();
                lii.cbSize = (uint)Marshal.SizeOf(typeof(LASTINPUTINFO));
                GetLastInputInfo(ref lii);
                return lii.dwTime;
            }
        }
    }

    public class IdleStateFactory
    {
        private TimeSpan _activeThreshold;
        private IdleTimeFactory _idleTimeFactory;

        public IdleStateFactory(IOptions<Worker.Settings> workerSettings, IdleTimeFactory idleTimeFactory)
        {
            _activeThreshold = TimeSpan.FromMilliseconds(workerSettings.Value.PollingInterval * 2);
            _idleTimeFactory = idleTimeFactory;
        }

        public virtual TimeSpan GetIdleTime()
        {
            var idleTime = _idleTimeFactory.IdleTime;
            if (idleTime.TotalMilliseconds > _activeThreshold.TotalMilliseconds)
                return idleTime;
            return TimeSpan.Zero;
        }
    }
}
