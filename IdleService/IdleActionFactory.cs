﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleService
{
    public class IdleActionFactory
    {
        public class Settings
        {
            public Dictionary<string, IdleAction.Settings> IdleActions { get; set; }
        }

        private Settings _settings;
        private IHttpActionExecutorFactory _factory;
        private ILoggerFactory _loggerFactory;


        public IdleActionFactory(IOptions<Settings> options, IHttpActionExecutorFactory actionFactory, ILoggerFactory loggerFactory)
        {
            _settings = options.Value;
            _factory = actionFactory;
            _loggerFactory = loggerFactory;
        }

        public IList<IdleAction> GetActions()
        {
            var result = new List<IdleAction>();

            foreach (var kvp in _settings.IdleActions)
            {
                var o = Options.Create(kvp.Value);
                var action = new IdleAction(o, _factory, _loggerFactory.CreateLogger<IdleAction>());
                action.Name = kvp.Key;

                result.Add(action);
            }

            return result;
        }

    }
}
