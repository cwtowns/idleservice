﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleService
{
    public interface IHttpActionExecutorFactory
    {
        IHttpActionExectuor GetHttpActionExecutor();
    }


    public class HttpActionExecutorFactory : IHttpActionExecutorFactory
    {
        private ILogger<BasicHttpActionExecutor> _logger;

        public HttpActionExecutorFactory(ILogger<BasicHttpActionExecutor> logger)
        {
            _logger = logger;
        }

        public virtual IHttpActionExectuor GetHttpActionExecutor()
        {
            return new BasicHttpActionExecutor(_logger);
        }
    }
}
