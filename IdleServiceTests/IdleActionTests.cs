using IdleService;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace IdleServiceTests
{
    [TestClass]
    public class IdleActionTests
    {
        [TestMethod]
        public async Task IdleActionTriggers()
        {
            var mockFactory = new Mock<IHttpActionExecutorFactory>();
            var mockAction = new Mock<IHttpActionExectuor>();
            var mockLogger = new Mock<ILogger<IdleAction>>();

            mockFactory.Setup(x => x.GetHttpActionExecutor()).Returns(mockAction.Object);

            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                IdleActionUrls = new List<string> { "http://foo.com" }
            };

            var options = Options.Create(settings);
            var action = new IdleAction(options, mockFactory.Object, mockLogger.Object);

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(settings.IdleThreshold + 1));

            Assert.IsTrue(action.IsCurrentlyIdle, "action should be in idle state because we are idle longer than the threshold");

            foreach (var url in settings.IdleActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Once, "All urls should be called if we're triggered.");
            }
        }

        [TestMethod]
        public async Task UserReturnsTriggers()
        {
            var mockFactory = new Mock<IHttpActionExecutorFactory>();
            var mockAction = new Mock<IHttpActionExectuor>();
            var mockLogger = new Mock<ILogger<IdleAction>>();

            mockFactory.Setup(x => x.GetHttpActionExecutor()).Returns(mockAction.Object);

            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                ActiveActionUrls = new List<string> { "http://foo.com" }
            };

            var options = Options.Create(settings);
            var action = new IdleAction(options, mockFactory.Object, mockLogger.Object);

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(0));

            Assert.IsFalse(action.IsCurrentlyIdle, "We should not be considered idle due to init first trigger logic");

            foreach (var url in settings.ActiveActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Once, "All urls should be called if we're triggered.");
            }
        }

        [TestMethod]
        public async Task UserReturnsDoesNotTriggerTwice()
        {
            var mockFactory = new Mock<IHttpActionExecutorFactory>();
            var mockAction = new Mock<IHttpActionExectuor>();
            var mockLogger = new Mock<ILogger<IdleAction>>();

            mockFactory.Setup(x => x.GetHttpActionExecutor()).Returns(mockAction.Object);

            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                ActiveActionUrls = new List<string> { "http://foo.com" }
            };

            var options = Options.Create(settings);
            var action = new IdleAction(options, mockFactory.Object, mockLogger.Object);

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(0));

            Assert.IsFalse(action.IsCurrentlyIdle, "user should not be idle");

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(0));

            Assert.IsFalse(action.IsCurrentlyIdle, "user should still not be idle");

            foreach (var url in settings.ActiveActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Once, "All urls should be called only once.  A second trigger should not have processed.");
            }
        }

        [TestMethod]
        public async Task IdleActionDoesNotTriggerTwice()
        {
            var mockFactory = new Mock<IHttpActionExecutorFactory>();
            var mockAction = new Mock<IHttpActionExectuor>();
            var mockLogger = new Mock<ILogger<IdleAction>>();

            mockFactory.Setup(x => x.GetHttpActionExecutor()).Returns(mockAction.Object);

            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                IdleActionUrls = new List<string> { "http://foo.com" }
            };

            var options = Options.Create(settings);
            var action = new IdleAction(options, mockFactory.Object, mockLogger.Object);

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(settings.IdleThreshold + 1));

            Assert.IsTrue(action.IsCurrentlyIdle, "action should be in idle state because we are idle longer than the threshold");

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(settings.IdleThreshold + 1));
            Assert.IsTrue(action.IsCurrentlyIdle, "action should still be in idle state.");

            foreach (var url in settings.IdleActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Once, "All urls should be called if we're triggered state change.");
            }
        }

        [TestMethod]
        public async Task ActionTriggerThrowingAnException()
        {
            var mockFactory = new Mock<IHttpActionExecutorFactory>();
            var mockAction = new Mock<IHttpActionExectuor>();
            var mockLogger = new Mock<ILogger<IdleAction>>();

            mockFactory.Setup(x => x.GetHttpActionExecutor()).Returns(mockAction.Object);

            mockAction.Setup(x => x.GetAsync(It.IsAny<string>())).Throws(new Exception());

            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                IdleActionUrls = new List<string> { "http://foo.com" }
            };

            var options = Options.Create(settings);
            var action = new IdleAction(options, mockFactory.Object, mockLogger.Object);

            Assert.IsFalse(action.IsCurrentlyIdle, "Actions should start not in idle state.");

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(settings.IdleThreshold + 1));

            Assert.IsFalse(action.IsCurrentlyIdle, "action should not have changed state because we errored.");
        }

        [TestMethod]
        public async Task StateChangesCorrectly()
        {
            var mockFactory = new Mock<IHttpActionExecutorFactory>();
            var mockAction = new Mock<IHttpActionExectuor>();
            var mockLogger = new Mock<ILogger<IdleAction>>();

            mockFactory.Setup(x => x.GetHttpActionExecutor()).Returns(mockAction.Object);

            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                IdleActionUrls = new List<string> { "http://foo.com" },
                ActiveActionUrls = new List<string> { "http://bar.com" }
            };

            var options = Options.Create(settings);
            var action = new IdleAction(options, mockFactory.Object, mockLogger.Object);

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(settings.IdleThreshold + 1));

            Assert.IsTrue(action.IsCurrentlyIdle, "user should be idle");

            foreach (var url in settings.IdleActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Once, "All urls should be called if we're triggered.");
            }

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(settings.IdleThreshold + 1));
            Assert.IsTrue(action.IsCurrentlyIdle, "action should still be in idle state");

            foreach (var url in settings.IdleActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Once, "Call count should not have increased");
            }

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(0));
            Assert.IsFalse(action.IsCurrentlyIdle, "user should now be idle");

            foreach (var url in settings.ActiveActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Once, "Active urls should have been called");
            }

            await action.ProcessIdleTime(TimeSpan.FromMilliseconds(settings.IdleThreshold + 1));

            Assert.IsTrue(action.IsCurrentlyIdle, "action should be in idle state");

            foreach (var url in settings.IdleActionUrls)
            {
                mockAction.Verify(m => m.GetAsync(It.Is<string>(s => s.Equals(url))), Times.Exactly(2), "Call count should have increased");
            }
        }

        [TestMethod]
        public void MalformedSettingsErrors()
        {
            var errored = false;
            try
            {
                new IdleAction.Settings()
                {
                    IdleThreshold = 10,
                    IdleActionUrls = new List<string> { "http://foo.{env:com" },
                    ActiveActionUrls = new List<string> { "http://bar.com" }
                };
            }
            catch (ArgumentOutOfRangeException)
            {
                errored = true;
            }

            Assert.IsTrue(errored, "Malformed urls should error");
        }

        [TestMethod]
        public void EnvironmentVariablesAreReplaced()
        {
            Environment.SetEnvironmentVariable("bar", "com");
            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                IdleActionUrls = new List<string> { "http://foo.{env:bar}/" },
                ActiveActionUrls = new List<string> { "http://foo.{env:bar}/" }
            };

            Assert.AreEqual("http://foo.com/", settings.IdleActionUrls[0], "Environment variables should be replaced.");
            Assert.AreEqual("http://foo.com/", settings.ActiveActionUrls[0], "Environment variables should be replaced.");
        }

        [TestMethod]
        public void MissingEnvironmentVariablesAreUnchanged()
        {
            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                IdleActionUrls = new List<string> { "http://foo.{env:bog}" },
                ActiveActionUrls = new List<string> { "http://foo.{env:bog}" }
            };

            Assert.AreEqual("http://foo.{env:bog}", settings.IdleActionUrls[0], "Missing environment variables should keep their markup.");
            Assert.AreEqual("http://foo.{env:bog}", settings.ActiveActionUrls[0], "Missing environment variables should keep their markup.");
        }

        [TestMethod]
        public void NoParametersStayTheSame()
        {
            var settings = new IdleAction.Settings()
            {
                IdleThreshold = 10,
                IdleActionUrls = new List<string> { "http://foo.com" },
                ActiveActionUrls = new List<string> { "http://foo.com" }
            };

            Assert.AreEqual("http://foo.com", settings.IdleActionUrls[0], "No parameters should remain unchanged.");
            Assert.AreEqual("http://foo.com", settings.ActiveActionUrls[0], "No parameters should remain unchanged.");
        }
    }
}
