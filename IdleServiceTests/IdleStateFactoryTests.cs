﻿using IdleService;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;


namespace IdleServiceTests
{

    [TestClass]
    public class IdleStateFactoryTests
    {
        [TestMethod]
        public void IgnoreLowIdleTimes()
        {
            var mockFactory = new Mock<IdleTimeFactory>();
            mockFactory.SetupGet(s => s.IdleTime).Returns(TimeSpan.FromSeconds(1));

            var options = Options.Create(new Worker.Settings
            {
                PollingInterval = Convert.ToInt32(TimeSpan.FromSeconds(1).TotalMilliseconds)
            }); ;

            var idleStateFactory = new IdleStateFactory(options, mockFactory.Object);

            Assert.AreEqual(0, idleStateFactory.GetIdleTime().TotalMilliseconds, "We have not idled past the threshold so we should consider the user not idle.");
        }

        [TestMethod]
        public void ReportIdleWhenPastThreshold()
        {
            var mockFactory = new Mock<IdleTimeFactory>();
            mockFactory.SetupGet(s => s.IdleTime).Returns(TimeSpan.FromSeconds(3));

            var options = Options.Create(new Worker.Settings
            {
                PollingInterval = Convert.ToInt32(TimeSpan.FromSeconds(1).TotalMilliseconds)
            }); ;

            var idleStateFactory = new IdleStateFactory(options, mockFactory.Object);

            Assert.AreNotEqual(0, idleStateFactory.GetIdleTime().TotalMilliseconds, "We have idled past the polling thereshold so we should consider the user idle");
        }
    }
}
