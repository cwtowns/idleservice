using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using IdleService;
using Serilog;
using Microsoft.Extensions.DependencyInjection;

namespace IdleServiceSysTrayApp
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {


            var host = Host.CreateDefaultBuilder()
             .ConfigureAppConfiguration((context, builder) =>
             {
                 // Add other configuration files...
                 builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                 builder.AddEnvironmentVariables();
             })
             .ConfigureServices((context, services) =>
             {
                 Log.Logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(context.Configuration)
                    .WriteTo.EventLog(source: "IdleService", manageEventSource: true)
                    .Enrich.FromLogContext()
                    .CreateLogger();

                 services.AddIdleService(context.Configuration);
                 services.AddSingleton<AppContext, AppContext>();
             })
             .UseSerilog()
             .Build();

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var services = host.Services;
            var app = services.GetRequiredService<AppContext>();
            Application.Run(app);
        }
    }
}
