﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using IdleService;
using System.Threading;

namespace IdleServiceSysTrayApp
{
    public class AppContext : ApplicationContext
    {
        private NotifyIcon _trayIcon;
        private CancellationTokenSource _startTokenSource = new CancellationTokenSource();
        private CancellationTokenSource _stopTokenSource = new CancellationTokenSource();
        private Worker _backgroundService;

        public AppContext(Worker backgroundService)
        {
            _backgroundService = backgroundService ?? throw new ArgumentNullException(nameof(backgroundService));

            var menu = new System.Windows.Forms.ContextMenuStrip();
            menu.Items.Add("Quit Idle Service", null, Exit);

            // Initialize Tray Icon
            _trayIcon = new NotifyIcon()
            {
                Icon = new Icon("IdleClock.ico"),
                ContextMenuStrip = menu,
                Visible = true,
                Text = "Idle Service"
            };

            _backgroundService.StartAsync(_startTokenSource.Token);
        }

        async void Exit(object sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            //_tokenSource.Cancel();

            await _backgroundService.StopAsync(_stopTokenSource.Token);

            _trayIcon.Visible = false;

            Application.Exit();
        }
    }
}
