# Idle Service
A windows service that triggers http requests when the user goes idle or comes back from being idle.  This is useful for some home automation tasks, like controlling lighting at your PC.

## Getting Started
Currently you have to clone and build.  I am not hosting msi binaries.  The app will be installed in `Program files (x86)` and have a shortcut in the Start Menu.  If you want it to autostart (likely) you need to manually add the shortcut to `shell:startup`.

Once installed, edit the [appsettings.json](https://gitlab.com/cwtowns/idleservice/-/raw/main/IdleServiceSysTrayApp/appsettings.json) file to configure.  Each object under `service.idleActions` is a toggle.  When the user goes idle past `idleThreshold`, the `idleActionsUrls` will be executed.  When they come back from being idle, the `activeActionUrls` will be executed.  URLs can have replacement parameters that pull from environment variables, for example `http://192.168.1.113/apps/api/191/devices/2/off?access_token={env:hubitatToken}`.

Informational events are written to Event Viewer under the IdleService application.  

### Lessons Learned

Originally I wanted this to be a windows service but services cannot successfully retrieve the user's idle time (or most things in the user space).  This isn't obvious when running the app from within VS.  The behavior only exists when you actually run as a service after defining it with `sc create`.

With a service being out I pivoted to a Winforms systray app (UWP and WPF cannot create Systray icons).  Winforms works well (and is the final tech used) but I was unable to get Gitlab CI/CD working with it.  Gitlab only hosts specific instances of Windows, and none of them support the target framework `net5.0-windows`.  I briefly explored hosting my own runner, but Docker requires the host and containers be the same OS version.  My host Windows OS is not supported by Gitlab and I would have to create a bridge image for this to work.  That sounded exhausting.

Creating the setup was difficult.  The setup project doesn't seem well suited to understanding dot net core applications.  I wanted to make a shortcut to the app for both the Start Menu and Start Up folder.  Doing this requires setting the shortcut Target correctly.  This happens for the Start Menu entry, but Start Up is wrong.  It doesn't have an Application Folder type that targets a project's exe, which would be host.exe when unbundled, or IdleServiceSysApp.exe when targetting self contained deployment.  Since neither worked you have to create a Start Up shortcut yourself.





